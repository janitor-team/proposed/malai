Malai
=====

libMalai is a Java implementation of the Malai architectural design pattern.
Malai can be viewed as an major step beyond MVC where the controller has been completely rethought to consider modern evolutions of the interactivity of systems.
Malai can also be viewed as MVP architecture focusing on modern concerns:
- More and more interactivity in software systems (with more and more post-WIMP interactions)
- Multi-platform development thanks to its modularity


Malai is based on the following HCI concepts: Norman's action model, instrumental interaction, direct manipulation, the interactor concept, and the DPI model.


Research papers on Malai:
- Blouin A. and Beaudoux O. Improving modularity and usability of interactive systems with Malai, EICS'10: Proceedings of the 2nd ACM SIGCHI symposium on Engineering interactive computing systems http://hal.archives-ouvertes.fr/docs/00/47/76/27/PDF/BLOUIN10a.pdf
- Blouin A., Morin B., Beaudoux O., Nain G., Albers P., and Jézéquel J.-M. Combining Aspect-Oriented Modeling with Property-Based Reasoning to Improve User Interface Adaptation, EICS'11: Proceedings of the 3rd ACM SIGCHI symposium on Engineering interactive computing systems, 85-94 http://hal.inria.fr/inria-00590891/PDF/main.pdf
- Blouin A. Un modèle pour l'ingénierie des systèmes interactifs dédiés à la manipulation de données, Ph.D. thesis http://tel.archives-ouvertes.fr/docs/00/44/63/14/PDF/these.pdf

